Gem::Specification.new do |s|
  s.name        = 'makebook'
  s.version     = '0.3.1'
  s.summary     = 'Creating books from Markdown'
  s.description = <<-EOS
   Assembling books with style from Markdown sources
  EOS
  s.authors     = ['Loic Nageleisen']
  s.email       = 'l.nageleisen@adhoc-gti.com'
  s.homepage    = 'https://gitlab.com/adhoc-gti/makebook'
  s.files       = Dir['lib/**/*.rb'] + Dir['bin/*']
  s.executables << 'makebook'

  s.add_dependency 'kramdown', '~> 1.9'
  s.add_dependency 'nokogiri', '~> 1.6'
  s.add_dependency 'tilt-pdf', '~> 0.10.0'
  s.add_dependency 'thor', '~> 0.19.1'

  s.add_development_dependency 'pry'
  s.add_development_dependency 'rubocop', '~> 0.46.0'
  s.add_development_dependency 'rake', '~> 10.5'
  s.add_development_dependency 'minitest', '~> 5.8'
end
