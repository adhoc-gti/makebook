require 'yaml'

# A book
class MakeBook::Book
  attr_reader :source

  def initialize(file)
    @source = Pathname.new(file)
  end

  private def book
    @book ||= YAML.load(File.read(source))
  end

  def root
    source.dirname
  end

  def name
    book['name'] || source.basename(MakeBook::EXT)
  end

  def dir
    root + name
  end

  def formats
    book['output']['formats'] || []
  end

  def title
    book['title'] || name
  end

  def chapters
    (book['chapters'] || []).map do |c|
      MakeBook::Chapter.new(book: self, name: c)
    end
  end

  def styles
    (book['styles'] || []).map do |s|
      MakeBook::Formats::HTML::Stylesheet.new(root + s)
    end
  end

  def toc
    book['toc'] && Integer(book['toc'])
  end

  def toc?
    !toc.nil?
  end
end
