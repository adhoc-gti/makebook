# Build context
class MakeBook::Build
  attr_reader :root

  def initialize(out: nil)
    @root = Pathname.new(out || MakeBook::Build.default_root)
  end

  def build(book)
    b = MakeBook::Book.new(book)
    b.formats.each do |format|
      format, options = format.first if format.is_a? Hash
      options ||= {}

      case format
      when 'html' then MakeBook::Formats::HTML.new(b, self, options).make
      when 'pdf' then MakeBook::Formats::PDF.new(b, self, options).make
      else raise NotImplementedError, format
      end
    end
  end

  class << self
    def default_root
      Pathname.pwd + 'out'
    end
  end
end
