# A book chapter
class MakeBook::Chapter
  attr_reader :source

  def initialize(file = nil, book: nil, name: nil)
    file ||= book.root + "#{name}.md"
    @source = Pathname.new(file)
  end

  def root
    source.dirname
  end
end
