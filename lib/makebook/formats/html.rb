require 'kramdown'
require 'nokogiri'
require 'base64'

# HTML output format
# rubocop:disable Metrics/ClassLength
class MakeBook::Formats::HTML
  attr_reader :book, :build

  def initialize(book, build, options = {})
    @book = book
    @build = build
    @options = options
  end

  # External element
  class Asset
    attr_reader :source, :mime

    def initialize(source, mime: nil)
      @source = source
      @mime = mime
    end

    def digest
      sha1 = Digest::SHA1.new
      sha1 << read
      sha1.hexdigest
    end

    def build_name
      "#{digest}#{source.extname}"
    end

    def read
      File.read(source)
    end

    def base64
      Base64.encode64(read)
    end

    def data_uri
      "data:#{mime};base64,#{base64}"
    end
  end

  # Wrap a string range for manipulation
  class Range
    def initialize(string, range)
      @string = string
      @range = range
    end

    def value=(string)
      @string[@range] = string
    end
  end

  # Stylesheet asset
  class Stylesheet < Asset
    TYPES = {
      'eot'  => 'application/vnd.ms-fontobject',
      'otf'  => 'application/font-sfnt',
      'ttf'  => 'application/font-sfnt',
      'woff' => 'application/font-woff',
      'png' => 'image/png',
      'jpg'  => 'image/jpg',
      'jpeg' => 'image/jpg',
      'svg'  => 'image/svg+xml',
      'gif'  => 'image/gif',
    }.freeze

    def initialize(source, mime: 'text/css')
      super
    end

    def assets
      scan(/url\("([^"]+\.(#{TYPES.keys.join('|')}))"\)/).map do |path, ext|
        [
          Asset.new(source.dirname + path, mime: TYPES[ext]),
          wrap(Regexp.last_match.begin(1)...Regexp.last_match.end(1)),
        ]
      end
    end

    def to_css
      read
    end

    def read
      @read ||= super
    end

    private

    def scan(re)
      read.enum_for(:scan, re)
    end

    def wrap(range)
      Range.new(read, range)
    end
  end

  # Section output
  class Section
    attr_reader :source

    def initialize(source)
      @source = source
    end

    def source_document
      Kramdown::Document.new(File.read(source),
                             input: 'GFM', hard_wrap: false)
    end

    def node
      @node ||= wrap(Nokogiri::HTML(source_document.to_html))
    end

    def to_html
      node.to_s
    end

    # rubocop:disable Metrics/AbcSize,Metrics/MethodLength
    def toc(levels, ordered: false)
      fragment = Nokogiri::HTML.fragment('')
      tag = ordered ? 'ol' : 'ul'

      toc_elements(levels).reduce([fragment, nil]) do |(level, last), header|
        if last.nil? || last.name < header.name
          level << node = Nokogiri::XML::Node.new(tag, fragment).tap do |ul|
            ul['class'] = header.name
          end
          level = node
        elsif last.name > header.name
          level = level.parent
        end

        level << Nokogiri::XML::Node.new('li', fragment).tap do |li|
          li['class'] = header.name
          li << Nokogiri::XML::Node.new('a', fragment).tap do |a|
            a['href'] = "##{header['id']}"
            a.content = header.text
          end
        end

        [level, header]
      end

      fragment
    end
    # rubocop:enable Metrics/AbcSize,Metrics/MethodLength

    private def toc_elements(levels)
      node.css(toc_selector(levels))
    end

    private def toc_selector(levels)
      Array.new(levels) { |i| "h#{i + 1}" }.join(', ')
    end

    def images
      node.css('img').map { |img| [Asset.new(root + img['src']), img] }
    end

    def root
      source.dirname
    end

    private def wrap(doc)
      doc.css('body').first.tap do |c|
        c.name = 'section'
        c['class'] = 'chapter'
      end
    end
  end

  def sections
    @sections ||= book.chapters.map { |c| Section.new(c.source) }
  end

  # rubocop:disable Metrics/AbcSize,Metrics/MethodLength
  def make
    build_dir.mkpath

    head << <<-HTML
      <title>#{book.title}</title>
    HTML

    book.styles.each do |stylesheet|
      stylesheet.assets.reverse_each do |asset, range|
        range.value = asset.build_name
        FileUtils.cp(asset.source, build_dir + asset.build_name)
      end

      File.open(build_dir + stylesheet.build_name, 'wb') do |f|
        f.write(stylesheet.to_css)
      end

      head << <<-HTML
        <link rel="stylesheet" href="#{build_dir.basename + stylesheet.build_name}" />
      HTML
    end

    header << book.title

    sections.each do |section|
      nav << section.toc(book.toc).to_s if book.toc?

      section.images.each do |asset, img|
        img['src'] = build_dir.basename + asset.build_name
        FileUtils.cp(asset.source, build_dir + asset.build_name)
      end

      main << section.to_html
    end

    open { |io| assemble(io) }
  end
  # rubocop:enable Metrics/AbcSize,Metrics/MethodLength

  # rubocop:disable Metrics/AbcSize,Metrics/MethodLength
  private def assemble(io)
    io.write("<!DOCTYPE html>\n")
    io.write(<<-EOS)
      <html>
        <head>
          <meta charset="utf-8" />
    EOS
    io.write(head)
    io.write(<<-EOS)
        </head>
        <body class='book'>
          <header>
    EOS
    io.write(header)
    io.write(<<-EOS)
          </header>
          <nav>
    EOS
    io.write(nav)
    io.write(<<-EOS)
          </nav>
          <main>
    EOS
    io.write(main)
    io.write(<<-EOS)
          </main>
          <footer>
    EOS
    io.write(footer)
    io.write(<<-EOS)
          </footer>
        </body>
      </html>
    EOS
  end
  # rubocop:enable Metrics/AbcSize,Metrics/MethodLength

  private def head
    @head ||= ''
  end

  private def header
    @header ||= ''
  end

  private def nav
    @nav ||= ''
  end

  private def main
    @main ||= ''
  end

  private def footer
    @footer ||= ''
  end

  def build_name
    build.root + book.root + book.name
  end

  def build_file
    Pathname("#{build_name}.html")
  end

  def build_dir
    build_name
  end

  def stale?
    book.source.mtime > build_file.mtime
  rescue Errno::ENOENT
    true
  end

  private def open
    File.open(build_file, 'wb') { |f| yield f }
  end
end
