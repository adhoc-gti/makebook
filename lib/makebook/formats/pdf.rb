require 'pdfkit'

# PDF output format
class MakeBook::Formats::PDF
  attr_reader :book, :build

  def initialize(book, build, options = {})
    @book = book
    @build = build
    @options = kit_options_from(options)
  end

  def make
    html = MakeBook::Formats::HTML.new(book, build)
    html.make if html.stale?

    kit = PDFKit.new(File.new(html.build_file), options)
    kit.to_file(build_file)
  end

  private def options
    {
      print_media_type: true,
      page_size: 'A4',
      margin_top: '2cm',
      margin_bottom: '2cm',
      margin_left: '2cm',
      margin_right: '2cm',
    }.merge(@options)
  end

  private def kit_options_from(options)
    options.dup.tap do |o|
      %w(header-html footer-html).each do |k|
        o[:"#{k}"] = uri_from(relative_path(o.delete(k))) if o.key?(k)
      end
    end
  end

  private def uri_from(path)
    "file://#{Pathname.pwd + path}"
  end

  private def relative_path(path)
    book.source + path
  end

  def build_name
    build.root + book.root + book.name
  end

  def build_file
    Pathname("#{build_name}.pdf")
  end

  def stale?
    book.source.mtime > build_file.mtime
  rescue Errno::ENOENT
    true
  end
end
