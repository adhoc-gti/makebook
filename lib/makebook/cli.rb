require 'thor'
require 'makebook'

# CLI commands
class MakeBook::CLI < Thor
  class_option :verbose, type: :boolean

  desc 'build [--out=DIRECTORY] [BOOK|...]', 'Make a book or more'
  options out: :optional
  def build(*books)
    build = MakeBook::Build.new(out: options[:out])

    books = Pathname.glob("**/*#{MakeBook::EXT}") if books.empty?

    books.each do |book|
      puts "building #{book}" if options[:verbose]
      build.build(book)
    end
  end
end
