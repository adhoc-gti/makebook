# Make books from Markdown
module MakeBook; end

require 'makebook/build'
require 'makebook/book'
require 'makebook/chapter'
require 'makebook/formats'

module MakeBook
  EXT = '.book.yml'.freeze
end
