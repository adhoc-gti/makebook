# MakeBook

## Usage

Installation:

```shell
$ gem install makebook
```

From the shell:

```shell
$ makebook build my/sample.book.yml  # builds one book
$ makebook build                     # builds all books, recursively
```

From ruby:

```ruby
build = MakeBook::Build.new(out: 'my/output/dir')
build.run('my/sample.book.yml')
```

## Tips

### Useful arguments of wkhtmltopdf

`wkhtmltopdf` supports the following interesting options:

- --outline / --outline-depth
- --debug-javascript
- --allow <path>

### Header and footer behaviour

HTML headers and footers get the following fields passed as query string:

- [page]       Replaced by the number of the pages currently being printed
- [frompage]   Replaced by the number of the first page to be printed
- [topage]     Replaced by the number of the last page to be printed
- [webpage]    Replaced by the URL of the page being printed
- [section]    Replaced by the name of the current section
- [subsection] Replaced by the name of the current subsection
- [date]       Replaced by the current date in system local format
- [isodate]    Replaced by the current date in ISO 8601 extended format
- [time]       Replaced by the current time in system local format
- [title]      Replaced by the title of the of the current page object
- [doctitle]   Replaced by the title of the output document
- [sitepage]   Replaced by the number of the page in the current site being converted
- [sitepages]  Replaced by the number of pages in the current site being converted

Headers and footers are rendered for each page and layered over the page. both
are rendered within page margin areas: header is positioned bottom against the
top of the document (like `position: absolute; bottom: 0;`) and footer is
positioned top against the bottom of the document (like `position: absolute;
top: 0;`). As an example: setting `padding-bottom: 1cm` on the header will
push the header up, not make it grow down.
